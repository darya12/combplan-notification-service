package com.orioninc.combplannotificationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CombplanNotificationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CombplanNotificationServiceApplication.class, args);
	}

}
